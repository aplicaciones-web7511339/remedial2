from django.db import models

# Create your models here.
class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=20, verbose_name='Nombre')
    precio = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Precio')
    stock = models.IntegerField(verbose_name='Stock')
    imagen = models.ImageField(upload_to='imagenes/', verbose_name='Imagen', null=True)
    #descripcion = models.TextField()
    #categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE)
    #created_at = models.DateTimeField(auto_now_add=True)
    #updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        fila = "Nombre: " + self.nombre #+ " - " + "Precio: " + self.precio ... solo agarra texto
        return fila
    
    def delete(self, using=None, keep_parents=False):
        self.imagen.storage.delete(self.imagen.name)
        super().delete()


class Equipo(models.Model):
    id_equipo = models.AutoField(primary_key=True)
    nombre_equipo = models.CharField(max_length=30, verbose_name='Nombre')
    venue_equipo = models.CharField(max_length=50, verbose_name='Venue')
    owner_equipo = models.CharField(max_length=30, verbose_name='Owner')
    def __str__(self):
        fila = "Nombre: " + self.nombre_equipo + " - " + "Venue: " + self.venue_equipo + " - " + "Owner: " + self.owner_equipo
        return fila
    
    def delete(self, using=None, keep_parents=False):
        self.imagen.storage.delete(self.imagen.name)
        super().delete()


class Estadio(models.Model):
    id_estadio = models.AutoField(primary_key=True)
    nombre_estadio = models.CharField(max_length=30, verbose_name='Nombre')
    capacidad_estadio = models.IntegerField(verbose_name='Capacidad')
    tamano_estadio = models.TextField(verbose_name='Tamano')
    def __str__(self):
        fila = "Nombre: " + self.nombre_estadio
        return fila
    
    def delete(self, using=None, keep_parents=False):
        self.imagen.storage.delete(self.imagen.name)
        super().delete()

    
class Jugador(models.Model):
    id_jugador = models.AutoField(primary_key=True)
    nombre_jugador = models.CharField(max_length=30, verbose_name='Nombre')
    numero_jugador = models.IntegerField(verbose_name='Numero')
    posicion_jugador = models.TextField(verbose_name='Posicion')
    def __str__(self):
        fila = "Nombre: " + self.nombre_jugador
        return fila
    
    def delete(self, using=None, keep_parents=False):
        self.imagen.storage.delete(self.imagen.name)
        super().delete()
