from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Equipo, Estadio, Jugador
from .forms import EquipoForm, EstadioForm, JugadorForm

# Create your views here. primero vista luego url

def inicio(request):
    return render(request, "paginas/inicio.html")

def pag1(request):
    return render(request, 'paginas/pag1.html')

#cruds equipo

def equipos(request):
    equipos = Equipo.objects.all()
    return render(request, 'equipos/index_equipo.html', {'equipos':equipos})

def crear_equipo(request):
    formulario = EquipoForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('equipos')
    return render(request, 'equipos/crear_equipo.html', {'formulario':formulario})

def editar_equipo(request):
    formulario = EquipoForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('equipos')
    return render(request, 'equipos/editar_equipo.html', {'formulario':formulario})

def eliminar_equipo(request, id_equipo):
    equipo = Equipo.objects.get(id_equipo=id_equipo)
    equipo.delete()
    return render(request, 'equipos')

#cruds estadios

def estadios(request):
    estadios = Estadio.objects.all()
    return render(request, 'estadios/index_estadio.html', {'estadios':estadios})

def crear_estadio(request):
    formulario = EstadioForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('estadios')
    return render(request, 'estadios/crear_estadio.html', {'formulario':formulario})

def editar_estadio(request):
    formulario = EquipoForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('equipos')
    return render(request, 'equipos/editar_estadio.html', {'formulario':formulario})

def eliminar_estadio(request, id_estadio):
    estadio = Estadio.objects.get(id_estadio=id_estadio)
    estadio.delete()
    return render(request, 'estadios')

#cruds jugadores

def jugadores(request):
    jugadores = Jugador.objects.all()
    return render(request, 'jugadores/index_jugador.html', {'jugadores':jugadores})

def crear_jugador(request):
    formulario = JugadorForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('jugadores')
    return render(request, 'jugadores/crear_jugador.html', {'formulario':formulario})

def editar_jugador(request):
    formulario = EquipoForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('equipos')
    return render(request, 'equipos/editar_jugador.html', {'formulario':formulario})

def eliminar_jugador(request, id_jugador):
    jugador = Jugador.objects.get(id_jugador=id_jugador)
    jugador.delete()
    return render(request, 'jugadores')


