from django.urls import path
from . import views

from django.conf import settings
from django.contrib.staticfiles.urls import static

#primero es el url que quieres asignarle, luego es el nombre que esta en views, por ultimo es el nombre para llamar en otros lados?

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('pag1', views.pag1, name='pag1'),

    path('equipos', views.equipos, name='equipos'),
    path('equipos/crear', views.crear_equipo, name='crear_equipo'),
    path('equipos/editar', views.editar_equipo, name='editar_equipo'),
    path('eliminar_equipo/<int:id>', views.eliminar_equipo, name='eliminar_equipo'),

    path('estadios', views.estadios, name='estadios'),
    path('estadios/crear', views.crear_estadio, name='crear_estadio'),
    path('estadios/editar', views.editar_estadio, name='editar_estadio'),
    path('eliminar_estadio/<int:id>', views.eliminar_estadio, name='eliminar_estadio'),

    path('jugadores', views.jugadores, name='jugadores'),
    path('jugadores/crear', views.crear_jugador, name='crear_jugador'),
    path('jugadores/editar', views.editar_jugador, name='editar_jugador'),
    path('eliminar_jugador/<int:id>', views.eliminar_jugador, name='eliminar_jugador'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
