from django.contrib import admin
from .models import Producto
from .models import Equipo
from .models import Estadio
from .models import Jugador

# Register your models here.

admin.site.register(Producto)
admin.site.register(Equipo)
admin.site.register(Estadio)
admin.site.register(Jugador)