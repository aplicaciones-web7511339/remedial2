from django import forms
from .models import Equipo, Estadio, Jugador


class EquipoForm(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = '__all__'

class EstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = '__all__'

class JugadorForm(forms.ModelForm):
    class Meta:
        model = Jugador
        fields = '__all__'

